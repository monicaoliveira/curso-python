import math
xA = int(input("Coordenadas do ponto A - Favor informar a coordenada  'x': "))
yA = int(input("Coordenadas do ponto A - Favor informar a coordenada  'y': "))
xB = int(input("Coordenadas do ponto B  - Favor informar a coordenada 'x'': "))
yB = int(input("Coordenadas do ponto B - Favor informar a coordenada 'y'': "))
xaoquadrado = (xA - xB) **2
yaoquadrado = (yA - yB) **2
distanciaAB = int(math.sqrt(xaoquadrado + yaoquadrado))
if distanciaAB >= 10:
    print("Distancia [", distanciaAB,"] Longe")
else:
    print("Distancia [", distanciaAB,"] Perto")
