#! Tarefa de programação: Lista de exercícios - 3
#! Atividade não enviada. Você deve obter 24/30 pontos para passar.
#! ================================================================
#! Exercício 1
#! Escreva um programa que receba um número natural n na entrada e imprima n! (fatorial) na saída.
#! Dica: lembre-se que o fatorial de 0 vale 1!
#! Digite o valor de n: 5
#! 120
#! ================================================================
n = 1
while n != 0:
    n = int(input("Digite o valor de n ou ZERO para sair: "))
    v = n
    i = 1
    f = 1
    while i <= n:
        f = f * v
        v = v - 1
        i = i + 1
    print (f)
#! ================================================================
#! Exercício 2
#! Receba um número inteiro positivo na entrada e imprima os n primeiros números ímpares naturais. Para a saída, siga o formato do exemplo abaixo.
#! Digite o valor de n: 5
#! 1
#! 3
#! 5
#! 7
#! 9
#! ================================================================
n = 1
while n != 0:
    n = int(input("Digite o valor de n ou ZERO para sair: "))
    v = 1
    i = 1
    f = 0
    while i <= n:
        f = v % 2
        if f != 0:
            print (v)
            i = i + 1
        v = v + 1
#! ================================================================
#! Exercício 3
#! Escreva um programa que receba um número inteiro na entrada, calcule e imprima a soma dos dígitos deste número na saída
#! Dica: Para separar os dígitos, lembre-se: 
#! o operador "//" faz uma divisão inteira jogando fora o resto, ou seja, aquilo que é menor que o divisor; 
#! O operador "%" devolve apenas o resto da divisão inteira jogando fora o resultado, ou seja, tudo que é maior ou igual ao divisor.
#! Digite um número inteiro: 123
#! 6
#! ================================================================
n = '1'
while n != '0':
    n = input("Digite um numero inteiro ou ZERO para sair: ")
    v = len(n) - 1
    i = 0
    f = 0
    while i <= v:
        f = f + int(n[i])
        i = i + 1
    print(f) 