#!========================================================
#!Tarefa de programacao: Lista de excercicios - 2
#!
#! Exercício 1 - Par ou ímpar?
#! Receba um número inteiro na entrada e imprima
#! par quando o número for par ou
#! ímpar quando o número for ímpar.
#!========================================================
numeroInteiro = int(input("(Par ou Ímpar?) Favor informar um número: "))
restoDivisao = numeroInteiro % 2
if (restoDivisao > 0):
    print("É ÍMPAR")
else:
    print("É PAR")
#!========================================================
#! Exercício 2 - FizzBuzz parcial parte 1
#! Receba um número inteiro na entrada e imprima
#! Fizz se o número for divisível por 3.
#! Caso contrário, imprima o mesmo número que foi dado na entrada.
#!========================================================
numeroInteiro = int(input("(Divisivel por 3?) Favor informar um número: "))
restoDivisao = numeroInteiro % 3
if (restoDivisao > 0):
    print(numeroInteiro)
else:
    print("Fizz")
#!========================================================
#! Exercício 3 - FizzBuzz parcial, parte 2
#! Receba um número inteiro na entrada e imprima
#! Buzz se o número for divisível por 5.
#! Caso contrário, imprima o mesmo número que foi dado na entrada.
#!========================================================
numeroInteiro = int(input("(Divisivel por 5?) Favor informar um número: "))
restoDivisao = numeroInteiro % 5
if (restoDivisao > 0):
    print(numeroInteiro)
else:
    print("Buzz")
#!========================================================
#! Exercício 4 - FizzBuzz parcial, parte 3 
#! Receba um número inteiro na entrada e imprima
#! FizzBuzz na saída se o número for divisível por 3 e por 5.
#! Caso contrário, imprima o mesmo número que foi dado na entrada.
#!========================================================
numeroInteiro = int(input("(Divisivel por 3 e por 5?) Favor informar um número: "))
restoDivisao3 = (numeroInteiro % 3)
restoDivisao5 = (numeroInteiro % 5)
if (restoDivisao3 > 0 or restoDivisao5 > 0):
    print(numeroInteiro)    
else:
    print("FizzBuzz")
#!========================================================
#! Exercício 5 - Verificando ordenação
#! Receba 3 números inteiros na entrada e imprima
#! crescente se eles forem dados em ordem crescente.
#! Caso contrário, imprima não está em ordem crescente
#!========================================================
n1 = int(input("(Em ordem crescente?) Favor informar primeiro número: "))
n2 = int(input("Favor informar segundo número: "))
n3 = int(input("Favor informar terceiro número: "))
emOrdemCrescente = (n1 < n2)  and (n2 < n3)
if (emOrdemCrescente):
    print("Crescente")    
else:
    print("não está em ordem crescente")
