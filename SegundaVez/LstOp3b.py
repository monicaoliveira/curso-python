#!Praticar tarefa de programação: Exercícios adicionais (opcionais)
#!=================================================================
#!Exercício 1
#!Escreva um programa que receba um número inteiro positivo na entrada e verifique se é primo. Se o número for primo, imprima "primo". Caso contrário, imprima "não primo".
#!Exemplos:
#!Digite um número inteiro: 13
#!primo
#!Digite um número inteiro: 12
#!não primo
#!=================================================================
#!Crivo de Eratóstenes
#!Um matemático grego chamado Eratóstenes (285-194 a.C) criou um sistema simples e objetivo para #!descobrir números primos, que foi chamado de crivo de Eratóstenes. Para representar a forma de #!utilizar o crivo, vamos considerar uma tabela com os números naturais de 1 a 100.
#!1º passo: localizar o primeiro número primo da tabela, que é o 2;
#!2º passo: marcar todos os múltiplos desse número;
#!3º passo: localizar o segundo número primo (3) e marcar todos os seus múltiplos;
#!4º passo: Repetir a operação até o último número.
#!Na tabela dos 100 primeiros números naturais, destacamos em roxo os números primos entre 1 e 100. #!São eles: 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, #!89, 97.
#!=================================================================
resto = 0
primo = 0
posicao = 0
naoehprimo = False
numerodigitado = 1
numerosprimos = '0203050711131719232931374143475359616771737983899700'
while numerodigitado != 0:
    numerodigitado = int(input("Digite um numero inteiro ou ZERO para sair: "))
    naoehprimo = False
    primo = 99
    posicao = 0
    while primo > 1:
        primo = int(numerosprimos[posicao]+numerosprimos[posicao+1])
        if numerodigitado <= primo:
                primo =0
        #!
        resto = 0
        if primo > 1:
            if (numerodigitado !=  primo and numerodigitado > primo):
                resto = (numerodigitado % primo)        
                if resto == 0:
                    naoehprimo = True
                    primo = 0
                #!
            #!            
        #!
        posicao = posicao + 2                    
    #!
    if naoehprimo:
        print("não primo")
    else:
        print("primo")
    #!
#!
#!=================================================================
#!Exercício 2 - Desafio do vídeo "Repetição com while"
#!Escreva um programa que receba um número inteiro na entrada e verifique se o número recebido possui ao menos um dígito com um dígito adjacente igual a ele. Caso exista, imprima "sim"; se não existir, imprima "não".
#!Exemplos:
#!Digite um número inteiro: 12345
#!não
#!Digite um número inteiro: 1011
#!sim
#!=================================================================
numerodigitado = 9
while numerodigitado != '0':
    numerodigitado = input("Informe um numero ou ZEROS para sair: ")
    tamanho = len(numerodigitado)-1
    indice = 0
    possui = False
    while indice < tamanho:
        if numerodigitado[indice] == numerodigitado[indice+1]:
            possui = True
        #!
        indice = indice + 1
    #!
    if possui:
        print("Este numero possui numeros adjacentes iguais :-)")
    else:
        print("Este numero não possui numeros adjacentes iguais :-(")
    #!
#!