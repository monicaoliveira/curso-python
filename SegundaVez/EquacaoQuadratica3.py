import math
a = int(input("Favor informar o valor de 'a': "))
b = int(input("Favor informar o valor de 'b': "))
c = int(input("Favor informar o valor de 'c': "))
Delta = ((b**2) - (4*a*c))
if Delta < 0:
    print("O Delta é menor que zero, então a equação não possui raízes reais")
else:
    RaizDelta = math.sqrt(Delta)
    Bhaskara1 = ((-b + RaizDelta) / (2*a))
    if Delta > 0:
        Bhaskara2 = ((-b - RaizDelta) / (2*a))
        print("O Delta é maior que zero, então a equação terá duas raízes reais e distintas")
        print("A primeira raíz de 'x' é [", Bhaskara1, "]")
        print("A segunda raíz de 'x' é [", Bhaskara2, "]")
    else:    
        print("O Delta é igual à zero, então a equação apresentará uma raíz real")
        print("A raíz de 'x' é [", Bhaskara1, "]")
