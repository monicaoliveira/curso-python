import math

print("")
print("Equação quadrática")
print("==================")
print("ax2 + bx + c = 0")
print("")
print("")
print("")
print("Formula de Bhaskara")
print("===================")
print("x = -b +- RAIZ de b.2 - 4.a.c")
print("    ----------------------")
print("               2.a         ")
print("")
a = int(input("Favor informar o valor de 'a': "))
b = int(input("Favor informar o valor de 'b': "))
c = int(input("Favor informar o valor de 'c': "))
print("")
Delta = ((b**2) - 4*a*c)
if Delta > 0:
    RaizDelta = math.sqrt(Delta)
    print("O Delta é maior que zero, então a equação terá duas raízes reais e distintas")
if Delta == 0:
    RaizDelta = math.sqrt(Delta)
    print("O Delta é igual à zero, então a equação apresentará uma raíz real")
if Delta < 0:
    RaizDelta = 0
    print("O Delta é menor que zero, então a equação não possui raízes reais")

Bhaskara1 = ((-b + RaizDelta) / 2*a)
Bhaskara2 = ((-b - RaizDelta) / 2*a)
            
print("")
print("Formula de Bhaskara")
print("===================")
print("A primeira raíz de 'x' é [", Bhaskara1, "]")
print("A segunda raíz de 'x' é [", Bhaskara2, "]")
print("")
print("")

EquacaoQuadratica1 = ( (a*(Bhaskara1**2)) + (b*Bhaskara1) + c )
EquacaoQuadratica2 = ( (a*(Bhaskara2**2)) + (b*Bhaskara2) + c )

print("")
print("Equação quadrática")
print("==================")
print("A primeira equação é [", EquacaoQuadratica1, "]")
print("A segunda equação é [", EquacaoQuadratica2, "]")
print("")
print("")
