import math
a = int(input("Favor informar o valor de 'a': "))
b = int(input("Favor informar o valor de 'b': "))
c = int(input("Favor informar o valor de 'c': "))
Delta = ((b**2) - (4*a*c))
if Delta < 0:
    print("esta equação não possui raízes reais")
else:
    RaizDelta = math.sqrt(Delta)
    Bhaskara1 = ((-b + RaizDelta) / (2*a))
    if Delta == 0:
        print("a raiz desta equação é X onde ",Bhaskara1," é o valor da raiz")
    else:
        Bhaskara2 = ((-b - RaizDelta) / (2*a))
        if Bhaskara1 <= Bhaskara2:
            print("as raízes da equação são X e Y onde ", Bhaskara1, " e ", Bhaskara2, " são os valores das raízes")
        else:
            print("as raízes da equação são X e Y onde ", Bhaskara2, " e ", Bhaskara1, " são os valores das raízes")

