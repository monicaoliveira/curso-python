#!========================================================
#! Praticar tarefa de programação: Exercícios adicionais (opcionais): Lista de excercicios - 3
#!
#! Exercício 1 - Distância entre dois pontos
#! Receba 4 números na entrada, um de cada vez. Os dois primeiros devem corresponder, 
#! respectivamente, às coordenadas x e y de um ponto em um plano cartesiano. Os dois últimos devem
#! corresponder, respectivamente, às coordenadas x e y de um outro ponto no mesmo plano.
#! Calcule a distância entre os dois pontos. 
#! Se a distância for maior ou igual a 10, 
#! iMprima longe na saída. 
#! Caso o contrário, quando a distância for menor que 10, imprima perto
#! Dica: lembre-se que a fórmula da distância para dois pontos num plano cartesiano é a seguinte:
#! d(x, y) = RAIZ QUADRADA DE [ {(x1 - x2) AO QUADRADO} + {(y1 - y2) AO QUADRADO} ]
#!========================================================
import math
xA = int(input("Coordenadas do ponto A - Favor informar a coordenada  'x': "))
yA = int(input("Coordenadas do ponto A - Favor informar a coordenada  'y': "))
xB = int(input("Coordenadas do ponto B  - Favor informar a coordenada 'x'': "))
yB = int(input("Coordenadas do ponto B - Favor informar a coordenada 'y'': "))
xaoquadrado = (xA + xB) **2
yaoquadrado = (yA + yB) **2
distanciaAB = math.sqrt(xaoquadrado + yaoquadrado)
if distanciaAB >= 10:
    print("longe")
else:
    print("perto")

#!========================================================
#! Exercício 2 - Desafio da videoaula
#! Como pedido na videoaula desta semana, escreva um programa que calcula as raízes de uma equação #! do segundo grau.
#! O programa deve receber os parâmetros a, b, e c da equação ax^2 + bx + c ax 2 +bx+c, 
#! respectivamente, e imprimir o resultado na saída da seguinte maneira:
#! Quando não houver raízes reais imprima: esta equação não possui raízes reais
#! Quando houver apenas uma raiz real imprima: a raiz desta equação é X onde X é o valor da raiz
#! Quando houver duas raízes reais imprima: as raízes da equação são X e Y onde X e Y são os valor
#! das raízes.
#! Além disso, no caso de existirem 2 raízes reais, elas devem ser impressas em ordem crescente.
#! Exemplos: 
#! as raízes da equação são 1.0 e 2.0
#! as raízes da equação são -20
#!========================================================
#! em 22/08/2019 - primeira vez
#! O resultado dos testes com seu programa foi:
#! ***** [0.1 pontos]: Testando Báskara sem raízes reais - formato da resposta (a = 5, b = 3, c = 5) - Falhou *****
#! AssertionError: Sua resposta está correta, mas não segue o formato pedido; verifique maiúsculas/minúsculas.
#! ***** [0.1 pontos]: Testando Báskara com uma raiz real - formato da resposta (a = 9, b = -12, c = 4) - Falhou *****
#! AssertionError: Sua resposta está correta, mas não segue o formato pedido; verifique maiúsculas/minúsculas; verifique os acentos; verifique os espaços.
#! ***** [0.2 pontos]: Testando Báskara com 2 raízes reais - formato da resposta (a = 1, b = -3, c = -10) - Falhou *****
#! AssertionError: Sua resposta está correta, mas não segue o formato pedido; verifique maiúsculas/minúsculas; verifique os acentos; verifique os espaços.
#!========================================================
#! em 22/08/2019 - segunda  vez
O resultado dos testes com seu programa foi:
#! ***** [0.1 pontos]: Testando Báskara com uma raiz real - formato da resposta (a = 9, b = -12, c = 4) - Falhou *****
#! AssertionError: Sua resposta está correta, mas não segue o formato pedido; verifique problemas com pontuação, espaços duplos ou erros de ortografia. A
#! resposta obtida foi "a raiz desta equação é X onde  0  é o valor da raiz"
#! ***** [0.2 pontos]: Testando Báskara com 2 raízes reais - formato da resposta (a = 1, b = -3, c = -10) - Falhou *****
#! AssertionError: Sua resposta está correta, mas não segue o formato pedido; verifique problemas com pontuação, espaços duplos ou erros de ortografia. A
#! resposta obtida foi "as raízes da equação são X e Y onde  -2  e  5  são os valores das raízes"
#!========================================================
import math
a = int(input("Favor informar o valor de 'a': "))
b = int(input("Favor informar o valor de 'b': "))
c = int(input("Favor informar o valor de 'c': "))
Delta = ((b**2) - (4*a*c))
if Delta < 0:
    print("esta equação não possui raízes reais")
else:
    RaizDelta = math.sqrt(Delta)
    Bhaskara1 = ((-b + RaizDelta) / (2*a))
    if Delta == 0:
        print("a raiz desta equação é X onde ",Bhaskara1," é o valor da raiz")
    else:
        Bhaskara2 = ((-b - RaizDelta) / (2*a))
        if Bhaskara1 <= Bhaskara2:
            print("as raízes da equação são X e Y onde ", Bhaskara1, " e ", Bhaskara2, " são os valores das raízes")
        else:
            print("as raízes da equação são X e Y onde ", Bhaskara2, " e ", Bhaskara1, " são os valores das raízes")

