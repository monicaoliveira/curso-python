v_ss_input = int(input("Por favor, entre com o número de segundos que deseja converter:"))
v_dd = (v_ss_input // 86400) # calcula divisao inteira, transformando em DIAS
v_dd_ss_resto = (v_ss_input % 86400) # calcula o resto de segundos em DIAS
v_hh = (v_dd_ss_resto // 3600) # calcula divisao inteira, transformando em HORAS
v_hh_ss_resto = (v_dd_ss_resto % 3600) # calcula o resto de segundos em HORAS
v_mm = (v_hh_ss_resto // 60) # calcula o resto da HORA, transformando em MINUTOS
v_ss = (v_hh_ss_resto % 60 ) # calcula o resto do da HORA, transforamndo em SEGUNDOS
print(v_dd, "dias,", v_hh, "horas,", v_mm, "minutos e", v_ss, "segundos.")
