# ========================================================================
# Praticar tarefa de programação: Exercícios adicionais (opcionais)
# Exercício 1
# ========================================================================
# Nome Monica Castro de Oliveira 
# Data 20/03/2019
# Hora 17:00
# ========================================================================
# Uma empresa de cartão de crédito envia suas faturas por email 
# com a seguinte mensagem:
# Olá, Fulano de Tal
# A sua fatura com vencimento em 9 de Janeiro no 
# valor de R$ 350,00 está fechada.
# Escreva um programa que receba (entrada de dados através do teclado) 
# o nome do cliente, o dia de vencimento, o mês de vencimento 
# e o valor da fatura e imprima (saída de dados) a mensagem 
# com os dados recebidos, no mesmo formato da mensagem acima. 
# Note que o programa imprime a saída em duas linhas diferentes. 
# Note também que, como não é preciso realizar cálculos, o valor não 
# precisa ser convertido para número, pode ser tratado como texto.
# ========================================================================
v_cliNome = input("Digite o nome do cliente: ")
v_vencDD = input("Digite o dia de vencimento: ")
v_vencMM = input("Digite o mês de vencimento: ")
v_valFat = input("Digite o valor da fatura: ")
print("Olá,", v_cliNome)
print("A sua fatura com vencimento em", v_vencDD, "de", v_vencMM, "no valor de R$ ", v_valFat, "está fechada.")
# ========================================================================
