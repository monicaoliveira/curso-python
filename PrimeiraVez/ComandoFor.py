# Iteração simplifica o programa de tartaruga
# Para desenhar um quadrado gostaríamos de fazer a 
# mesma coisa quatro vezes — mover a tartaruga alguma 
# distância para frente e girar 90 graus. 
# Anteriormente usamos 8 linhas de código Python para 
# fazer edgar desenhar os quatro lados de um quadrado. 
# Este próximo programa faz exatamente a mesma coisa mas, 
# com a ajuda do comando for, usa apenas três linhas (
# não incluindo o código de inicialização). 
# Lembre-se que o comando for repetirá o forward
#  e left quatro vezes, uma vez para cada valor na lista.
# Nesse caso, o valor de aColor é usado para modificar o 
# atributo cor de alex. Cada iteração causa a modificação de
# aColor para o próximo valor da lista.

import turtle            # cria edgar
wn = turtle.Screen()
edgar = turtle.Turtle()

for aColor in ["yellow", "red", "purple", "blue"]: # repita 4 vezes
   edgar.color(aColor)
   edgar.forward(50)
   edgar.left(90)

wn.exitonclick()

