# A função range
# No exemplo da última sessão (mostrado novamente abaixo), 
# usamos uma lista de quatro inteiros para causar que a
#  iteração se repetisse quatro vezes. Dissemos que poderíamos ter
#  usado quatro valores quaisquer. De fato, até usamos quatro 
# cores.

import turtle            # cria edgar
wn = turtle.Screen()
edgar = turtle.Turtle()

for i in [0,1,2,3]:      # repita 4 vezes
    print("Sem array", i)
    edgar.color("blue")
    edgar.forward(50)
    edgar.left(90)

# Acontece que a geração de listas com um número específico
#  de inteiros é uma coisa muito comum de se fazer,
#  especialmente quando você quer escrever uma iteração
#  simples controlada por um laço for. Mesmo que você possa
#  usar quatro itens quaisquer, ou quaisquer quatro números inteiros,
#  o convencional é usar uma lista de inteiros que começa com 0.
#  Na verdade, essas listas são tão populares que o Python fornece um
#  objeto range (intervalo) nativo que pode ser usado para fornecer uma
#  sequência de valores para o laço for. As sequências começam de 0 e 
# nos casos mostrados abaixo não incluem o 4 e o 10.
for i in range(4):
    print("Com array", i)
    edgar.color("purple")
    edgar.forward(100)
    edgar.left(90)

wn.exitonclick()