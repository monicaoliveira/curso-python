import turtle            # permite usar as funções e objetos do módulo turtle
wn = turtle.Screen()     # cria uma janela gráfica
edgar = turtle.Turtle()   # cria um turtle chamado edgar
edgar.forward(150)        # manda o edgar se mover 150 unidades para frente
edgar.left(90)            # roda de 90 graus para a esquerda
edgar.forward(75)         # desenha o segundo lado do retângulo
edgar.left(90)            # roda de 90 graus para a esquerda
edgar.forward(150)        # desenha o segundo lado do retângulo
edgar.left(90)            # roda de 90 graus para a esquerda
edgar.forward(75)         # desenha o segundo lado do retângulo
