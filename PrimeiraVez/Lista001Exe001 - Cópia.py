v_lado = int(input("Digite o valor correspondente ao lado de um quadrado:"))
v_perimetro = (v_lado * 4) # perímetro do quadrado é P = L * 4
v_area = (v_lado ** 2) # área do quadrado é A = L x L
print("perímetro:", v_perimetro, "- área:", v_area)
