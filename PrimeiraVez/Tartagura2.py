# ======================================================================
# lista de cores
# https://www.w3schools.com/colors/colors_names.asp
# ======================================================================

import turtle

wn = turtle.Screen()
wn.bgcolor("MediumAquaMarine")         # define a cor de fundo da janela

edgar = turtle.Turtle()
edgar.color("HotPink")               # edgar fica azul
edgar.pensize(8)                  # define a espessura da caneta

edgar.forward(150)        # manda o edgar se mover 150 unidades para frente
edgar.left(90)            # roda de 90 graus para a esquerda
edgar.forward(75)         # desenha o segundo lado do retângulo
edgar.left(90)            # roda de 90 graus para a esquerda
edgar.forward(150)        # desenha o segundo lado do retângulo
edgar.left(90)            # roda de 90 graus para a esquerda
edgar.forward(75)         # desenha o segundo lado do retângulo


wn.exitonclick()
