# Instâncias — um bando de tartarugas
# Da mesma maneira que podemos ter vários inteiros em um programa, 
# podemos ter várias tartarugas. Cada uma delas é um objeto 
# independente que chamamos de instância do tipo (classe) Turtle. 
# Cada instância tem os seus próprios atributos e métodos — assim
# edgar pode desenhar usando uma caneta preta fina e ficar em uma
# posição, enquanto monica pode ir em outra direção desenhando com 
# uma caneta rosa. O seguinte ocorre quando edgar completa um 
# quadrado e monica completa seu triângulo:

import turtle
wn = turtle.Screen()             # Cria uma janela e define seus atributos
wn.bgcolor("lightgreen")

monica = turtle.Turtle()           # cria monica e define seus atributos
monica.color("hotpink") # cor da linha
monica.pensize(5) # grossura da linha
monica.forward(80)                 # monica desenha um triângulo equilátero
monica.left(120)
monica.forward(80)
monica.left(120)
monica.forward(80)
monica.left(120)                   # complete o triângulo
monica.right(180)                  # monica muda de direção
monica.forward(80)                 # monica se move para longe da origem

edgar = turtle.Turtle()           # cria edgar
edgar.color("red") # cor da linha
edgar.pensize(8) # grossura da linha
edgar.forward(50)                 # edgar desenha um quadrado
edgar.left(90)
edgar.forward(50)
edgar.left(90)
edgar.forward(50)
edgar.left(90)
edgar.forward(50)
edgar.left(90)

wn.exitonclick()

