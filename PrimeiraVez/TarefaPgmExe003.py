# ========================================================================
# Praticar tarefa de programação: Exercícios adicionais (opcionais)
# Exercício 3 Este é o desafio do vídeo "Entrada de Dados
# ========================================================================
# Nome Monica Castro de Oliveira 
# Data 20/03/2019
# Hora 17:30
# ========================================================================
# Faça um programa em Python que recebe um número inteiro e 
# imprime seu dígito das dezenas.
# ========================================================================
# Valor 78615
# Divisão inteira por 10 = 7861
# Resto da divisao por 10 = 5
# O digito das dezenas é 1
# ========================================================================
v_valor = int(input("Digite um número inteiro: "))
v_quociente = str(v_valor // 10) # divisão por 10
v_resto = (v_valor % 10) # resto da divisãopor 10
v_digito = v_quociente[-1]
print("O dígito das dezenas é ", v_digito )
# ========================================================================
