# ========================================================================
# Lista de exercício 1 - Exercício 2
# ========================================================================
# Nome Monica Castro de Oliveira 
# Data 20/03/2019
# Hora 16:45
# ========================================================================
# Faça um programa em Python que receba quatro notas, calcule e 
# imprima a média aritmética.
# ========================================================================
v_nota1 = float(input("Digite a primeira nota:"))
v_nota2 = float(input("Digite a segunda nota:"))
v_nota3 = float(input("Digite a terceira nota:"))
v_nota4 = float(input("Digite a quarta nota:"))
v_media_aritmetica = (v_nota1+v_nota2+v_nota3+v_nota4)/4
print("A média aritmética é:", v_media_aritmetica)
# ========================================================================
