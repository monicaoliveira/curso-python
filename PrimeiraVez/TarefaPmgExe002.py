# ========================================================================
# Praticar tarefa de programação: Exercícios adicionais (opcionais)
# Exercício 2 Este é o desafio do vídeo "Entrada de Dados
# ========================================================================
# Nome Monica Castro de Oliveira 
# Data 20/03/2019
# Hora 17:15
# ========================================================================
# 1 dia = 86400 segundos
# 1 minuto = 60 segundos
# 1 hora = 3600 segundos
# ========================================================================
# Reescreva o programa contaSegundos para imprimir também a 
# quantidade de dias, ou seja, faça um programa em Python que, 
# dada a quantidade de segundos, "quebre" esse valor em dias, 
# horas, minutos e segundos. A saída deve estar no formato: 
# a dias, b horas, c minutos e d segundos. 
# Seja cuidadoso com o formato! Espaços a mais, vírgulas 
# faltando ou outras diferenças são considerados erro
# ========================================================================
v_ss_input = int(input("Por favor, entre com o número de segundos que deseja converter: "))
v_dd = (v_ss_input // 86400) # calcula divisao inteira, transformando em DIAS
v_dd_ss_resto = (v_ss_input % 86400) # calcula o resto de segundos em DIAS
v_hh = (v_dd_ss_resto // 3600) # calcula divisao inteira, transformando em HORAS
v_hh_ss_resto = (v_dd_ss_resto % 3600) # calcula o resto de segundos em HORAS
v_mm = (v_hh_ss_resto // 60) # calcula o resto da HORA, transformando em MINUTOS
v_ss = (v_hh_ss_resto % 60 ) # calcula o resto do da HORA, transforamndo em SEGUNDOS
print(v_dd, "dias", v_hh, "horas,", v_mm, "minutos e", v_ss, "segundos")
# ========================================================================
