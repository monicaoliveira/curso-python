# ========================================================================
# Lista de exercício 1 - Exercício 1
# ========================================================================
# Nome Monica Castro de Oliveira 
# Data 20/03/2019
# Hora 16:35
# ========================================================================
# Faça um programa em Python que receba (entrada de dados) o
# valor correspondente ao lado de um quadrado, calcule e imprima 
# (saída de dados) seu perímetro e sua área.
# Observação: a saída deve estar no formato: "perímetro: x - área: y"
# ========================================================================
v_lado = int(input("Digite o valor correspondente ao lado de um quadrado:"))
v_perimetro = (v_lado * 4) # perímetro do quadrado é P = L * 4
v_area = (v_lado ** 2) # área do quadrado é A = L x L
print("perímetro:", v_perimetro, " - área: ", v_area)
# ========================================================================
