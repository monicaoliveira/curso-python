#  C   =      F  -  32
# ---  =   --------------
#  5            9

temperaturaFarenheit = float(input("Digite uma temperatura em Farenheit: "))
temperaturaCelsius = (temperaturaFarenheit - 32) * 5 / 9

print('Quando a temperatura em Farenheit for ', temperaturaFarenheit)
print('A temperatura em Celsius será ', temperaturaCelsius)
